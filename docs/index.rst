.. SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ
..
.. SPDX-License-Identifier: GPL-3.0-or-later

.. include:: misc/title.rst


.. toctree::
   :maxdepth: 1
   :titlesonly:
   :hidden:

   Repository <https://git.ufz.de/rdm-software/saqc>
   Getting Started <gettingstarted/GettingStartedPanels>
   Guides <cookbooks/CookBooksPanels>
   Configurator <https://webapp.ufz.de/saqc-config-app/>
   Documentation <documentation/documentationPanels>
   Functions API <funcs/funcsPanels>
   Developer Resources <devresources/devResPanels>


.. toctree::
   :caption: SaQC
   :hidden:
   :maxdepth: 1

   Package <modules/SaQC>
   Core <modules/SaQCCore>
   Test Functions <modules/SaQCFunctions>
   Translations <modules/SaQCTranslation>
   Constants <modules/SaQCConstants>


..
   Indices and tables
   ==================

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`

