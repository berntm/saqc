.. SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ
..
.. SPDX-License-Identifier: GPL-3.0-or-later

generic wrapper
===============


.. currentmodule:: saqc

.. autosummary::
   :nosignatures:

   ~SaQC.processGeneric
   ~SaQC.flagGeneric
   ~SaQC.roll
   ~SaQC.transform
   ~SaQC.resample
