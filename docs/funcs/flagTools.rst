.. SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ
..
.. SPDX-License-Identifier: GPL-3.0-or-later

flagtools
=========

.. currentmodule:: saqc

.. autosummary::

   ~SaQC.forceFlags
   ~SaQC.clearFlags
   ~SaQC.flagUnflagged
   ~SaQC.flagManual
   ~SaQC.flagDummy
   ~SaQC.transferFlags
